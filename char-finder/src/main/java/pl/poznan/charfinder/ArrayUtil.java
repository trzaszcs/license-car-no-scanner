package pl.poznan.charfinder;


public final class ArrayUtil {

    ArrayUtil() {
    }

    public static int getDistance(int[] array1, int[] array2) {
        if (array1.length != array2.length) {
            throw new IllegalArgumentException("different size");
        }
        int diff = 0;
        for (int c = 0; c < array1.length; c++) {
            diff += Math.abs(array1[c] - array2[c]);
        }
        return diff;
    }

}
