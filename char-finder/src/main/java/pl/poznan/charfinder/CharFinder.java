package pl.poznan.charfinder;


import java.util.Map;

public class CharFinder {
    private final Map<String, int[]> charsMap;
    private final int maxDistance;

    public CharFinder(Map<String, int[]> charsMap, int maxDistance) {
        this.charsMap = charsMap;
        this.maxDistance = maxDistance;
    }

    public String find(int[] charMatrix) {
        String winnerChar = null;
        int smallestDistance = Integer.MAX_VALUE;
        for (Map.Entry<String, int[]> entry : charsMap.entrySet()) {
            int currentDistance = ArrayUtil.getDistance(entry.getValue(), charMatrix);
            if (currentDistance < smallestDistance && currentDistance < maxDistance) {
                winnerChar = entry.getKey();
                smallestDistance = currentDistance;
            }
        }
        return winnerChar;
    }
}
