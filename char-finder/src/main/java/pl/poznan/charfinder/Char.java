package pl.poznan.charfinder;


import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class Char {
    private final Map<String, Integer[]> chars = ImmutableMap.of(
            "a", new Integer[]{
                    0, 0, 50, 0, 0,
                    0, 50, 0, 50, 0,
                    0, 50, 0, 50, 0,
                    10, 50, 100, 50, 10,
                    30, 30, 0, 30, 30,
                    50, 0, 0, 0, 50
            });


}
