package pl.poznan.charfinder

import org.junit.Test


class CharFinderTest {


    @Test
    void "should ignore too big distance"() {
        // g
        def charMatrix = [5] as int[]
        def charFinder = new CharFinder(["a": [1] as int[]], 2)
        // w
        def result = charFinder.find(charMatrix)
        // t
        assert result == null
    }

    @Test
    void "should find symobl"() {
        // g
        def charMatrix = [1] as int[]
        def charFinder = new CharFinder(["a": [3] as int[], "b": [2] as int[]], 2)
        // w
        def result = charFinder.find(charMatrix)
        // t
        assert result == "b"
    }


}
